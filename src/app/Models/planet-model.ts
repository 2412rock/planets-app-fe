import { PlanetStatus } from "./planet-status";

export class PlanetModel{
    public captainName: string;
    public description: string;
    public id: number;
    public image: string;
    public name: string;
    public robots: string;
    public status: PlanetStatus;
    
}