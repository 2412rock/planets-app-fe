import { Component } from '@angular/core';
import { PlanetModel } from './Models/planet-model';
import { PlanetsService } from './services/planets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'planetsApp';
  planetData: PlanetModel[];

  constructor(private planetsService: PlanetsService){}

  async ngOnInit(){
     (await this.planetsService.getAllPlanets()).subscribe((data) => {
      this.planetData = data;
     });
  }
}
