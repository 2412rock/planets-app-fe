import { PlanetStatus } from "./planet-status";

export class PlanetUpdate{
    public planetId: number;
    public planetDescription: string;
    public planetStatus: PlanetStatus;
}