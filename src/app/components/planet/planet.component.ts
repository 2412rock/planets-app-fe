import { Component, Input } from '@angular/core';
import { PlanetModel } from 'src/app/Models/planet-model';
import { PlanetStatus } from 'src/app/Models/planet-status';
import { faPenToSquare } from '@fortawesome/free-regular-svg-icons'
import { PlanetsService } from 'src/app/services/planets.service';
import { PlanetUpdate } from 'src/app/Models/planet-update';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent {

  @Input() planet: PlanetModel;

  public descriptionText: string;
  editIcon = faPenToSquare;
  editClicked: boolean = false;
  planetStatus = PlanetStatus;
  selectedStatus: PlanetStatus;
  showSpinner: boolean = false;

  constructor(private planetsService: PlanetsService){}

  ngOnInit() {
    this.selectedStatus = this.planet.status;
  }

  planetStatusArray(): Array<number> {
    let result = [];
    for (let i in this.planetStatus) {
      let nr = parseInt(i);
      if (!Number.isNaN(nr)) {
        result.push(nr);
      }
    }

    return result;
  }

  isGreenText(planetStatus: PlanetStatus) {
    if (planetStatus === PlanetStatus.OK) {
      return true;
    }
    return false;
  }

  isRedText(planetStatus: PlanetStatus) {
    if (planetStatus === PlanetStatus.NOT_OK) {
      return true;
    }
    return false;
  }

  isGreyText(planetStatus: PlanetStatus) {
    if (planetStatus === PlanetStatus.EN_ROUTE || planetStatus === PlanetStatus.TODO) {
      return true;
    }
    return false;
  }

  getStatusAsString(planetStatus: PlanetStatus | any) {
    switch (planetStatus) {
      case PlanetStatus.OK:
        return 'OK'
      case PlanetStatus.NOT_OK:
        return "!OK"
      case PlanetStatus.TODO:
        return "TO DO";
      case PlanetStatus.EN_ROUTE:
        return "En route"
      default:
        console.log(`Invalid status`);
        return ""
    }
  }

  onClickEdit(text: string) {
    this.editClicked = true;
    this.descriptionText = text;
  }

  async onClickSave() {
    this.showSpinner = true;
    this.editClicked = false;

    this.planet.description = this.descriptionText;
    this.planet.status = parseInt(this.selectedStatus.toString());

    let planetUpdate: PlanetUpdate = new PlanetUpdate();
    planetUpdate.planetId = this.planet.id;
    planetUpdate.planetDescription = this.planet.description;
    planetUpdate.planetStatus = this.planet.status;

    this.planetsService.updatePlanet(planetUpdate).subscribe((data) => {
     this.showSpinner = false;
    }, (res) => {
      console.log(res.status)
      if(res.status != 200){
        console.log("an error occured saving the data")
        this.showSpinner = false;
      }
    })
    
  }

}
