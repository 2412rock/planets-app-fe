import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PlanetModel } from '../Models/planet-model';
import { PlanetUpdateResponse } from '../Models/planet-update-response';
import { PlanetUpdate } from '../Models/planet-update';

@Injectable({
  providedIn: 'root'
})
export class PlanetsService {

  constructor(private http: HttpClient) {
   }

  getAllPlanets(): Observable<PlanetModel[]>{
    let response = this.http.get<PlanetModel[]>("https://192.168.1.236:45455/Planets");
    return response;
  }

  updatePlanet(planetUpdate: PlanetUpdate): Observable<HttpResponse<string>>{
    let response = this.http.put<string>("https://192.168.1.236:45455/Planets", planetUpdate, {observe: 'response'});
    return response;
  }
}
